# Containers

** Keyvan Hedayati **



# What?


* Processes that are isolated from the rest of the system
* An image with all files necessary to the processes
* Portable and consistent during
 * Development
 * Testing
 * Production


## Isn't this just virtualization? Yes and No

* **Virtualization:** Operating Systems runs simultaneously on a single system
* **Containers:** Share the same OS and isolate the processes from the rest of the system

![](imgs/virtualization-vs-containers.png)


It's called
## Operating-system-level virtualization



# Why?


## Problem

* Multiplicity of Stacks: ** Interaction and Integration **
![](imgs/the-challenge.png)
* Multiplicity of Hardware: ** Migration and Deployment **


## Matrix of Hell

![](imgs/the-matrix-from-hell.png)   <!-- .element: style="height: 500px;" -->


## Humanity had same problem before


## Problem

* Multiplicity of Goods: ** Interactio **
![](imgs/the-challenge.png)
* Multiplicity of Transfer and Storage: ** Migration and Deployment **
